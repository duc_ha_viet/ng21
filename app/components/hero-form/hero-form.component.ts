import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Hero }    from '../../entities/hero';

@Component({
    selector: 'hero-form',
    templateUrl: 'app/components/hero-form/hero-form.component.html'
})
export class HeroFormComponent implements OnInit{
    @Input()
    public addHero: Function;

    public addHeroForm: FormGroup;

    private defaultHeroId: number = 20;

    constructor(private formBuilder: FormBuilder) {}

    ngOnInit() {
        this.addHeroForm = this.formBuilder.group({
            name: ['', [Validators.required, Validators.maxLength(20)]],
            power: ['', Validators.required]
        });
    }

    onAddHero() {
        let formData = this.addHeroForm.value;
        this.defaultHeroId++;
        let hero = new Hero(this.defaultHeroId, formData.name);

        this.addHero(hero);
        this.addHeroForm.reset();
    }
}