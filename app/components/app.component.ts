import {Component, OnInit} from '@angular/core';
import {Hero} from '../entities/hero';
import {HeroService} from '../services/hero.service';
import {Store} from "@ngrx/store";
import {AppState} from "../entities/app-state";
import {Observable} from "rxjs";
import {HERO_LOAD_SUCCESSFUL, HERO_ADD_SUCCESSFUL, HERO_DELETE_SUCCESSFUL} from "../actions/heros.action";

@Component({
    selector: 'my-app',
    providers: [HeroService],
    templateUrl: 'app/components/app.component.html',
    styleUrls: ['app/components/app.component.css']
})
export class AppComponent implements OnInit{
    public title = 'Tour of Heroes';
    public heroes: Observable<any>;
    public selectedHero: Hero;

    constructor(private store: Store<AppState>, private heroService: HeroService) {
        this.heroes = store.select('heroes');
    }

    ngOnInit(): void {
        this.getHeroes();

        this.onAddHero = this.onAddHero.bind(this);
    }

    onSelect(hero: Hero): void {
        this.selectedHero = hero;
    }

    deleteHero(hero: Hero): void {
        this.store.dispatch({type: HERO_DELETE_SUCCESSFUL, payload: hero});
    }

    getHeroes(): void {
        this.heroService.getHeroes().then(heroes => {
            this.store.dispatch({type: HERO_LOAD_SUCCESSFUL, payload: heroes});
        });
    }

    onAddHero(hero: Hero): void {
        this.store.dispatch({type: HERO_ADD_SUCCESSFUL, payload: hero});
    }
}