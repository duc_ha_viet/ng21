/**
 * Created by Peter Hoang Nguyen on 9/13/2016.
 */

import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { AppComponent }  from './components/app.component';
import { HeroDetailComponent } from './components/hero-detail/hero-detail.component';
import { HeroFormComponent } from './components/hero-form/hero-form.component';
import { PowerFieldComponent } from './components/hero-form/power-field/power-field.component';
import { HighlightDirective } from './directive/highlight.directive';
import {StoreModule} from "@ngrx/store";
import {AppState} from "./entities/app-state";
import * as APP_REDUCERS from './reducers/reducers';

let initialState: AppState = {
    heroes: []
};

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        StoreModule.provideStore(APP_REDUCERS, initialState)
    ],
    declarations: [
        AppComponent,
        HeroDetailComponent,
        HeroFormComponent,
        PowerFieldComponent,
        HighlightDirective
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }